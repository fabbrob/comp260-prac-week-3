﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoveAccelerate : MonoBehaviour {

public float maxSpeed = 5.0f; // in metres per second
public float acceleration = 1.0f; // in metres/second/second
public float brake = 5.0f; // in metres/second/second
public float turnSpeed = 30.0f; // in degrees/second

public string Horizontal;
public string Vertical;

private float speed = 0.0f; // in metres/second

    // Use this for initialization
    void Start()
{

}

// Update is called once per frame
void Update()
{
    // the horizontal axis controls the turn
    float turn = Input.GetAxis(Horizontal);

    // turn the car
    transform.Rotate(0, 0, -turn * turnSpeed * speed * Time.deltaTime); //changed the sign of the turn so that the players would turn correctly
                                                                          // also added speed/3 to the calculation so that the turn speed would be proportional to their movement

    // the vertical axis controls acceleration fwd/back
    float forwards = Input.GetAxis(Vertical);

    if (forwards > 0)
    {
        // accelerate forwards
        speed = speed + acceleration * Time.deltaTime;
    }
    else if (forwards < 0)
    {
        // accelerate backwards
        speed = speed - acceleration * Time.deltaTime;
    }
    else
    {
        // braking
        if (speed > 0)
        {
                if((speed - brake * Time.deltaTime) < 0) //added in this condition so that when the player's speed == 0 the player wouldnt twitch up and down
                {
                    speed = 0;
                } else
                {
                    speed = speed - brake * Time.deltaTime;
                }
        }
        else if (speed < 0) 
        {
                if ((speed + brake * Time.deltaTime) > 0) //likewise as above, added this code to negate the twitching
                {
                    speed = 0;
                }
                else
                {
                    speed = speed + brake * Time.deltaTime;
                }
            }
    }

    // clamp the speed
    speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);
    // compute a vector in the up direction of length speed
    Vector2 velocity = Vector2.up * speed;

    // move the object
    transform.Translate(velocity * Time.deltaTime, Space.Self);
    }
}
